version: '3.8'

networks:
  confec-nets:
    name : confec-nets
    external : true

volumes:
  data-orderer.confectionary.com:
  data-shop1.confectionary.com:
  data-shop2.confectionary.com:
  data-mill.flourmill.com:
  data-ovens.bakingstuff.com:

services:
  development:
    container_name: depay7
    build:
      context: .
      dockerfile: Dockerfile.Dev
    # environment:
    command: sleep infinity
    stop_grace_period: 5s
    volumes:
      - ../..:/workspaces:cached
    # depends_on:
    networks:
      - confec-nets

  orderer.confectionary.com:
    container_name: orderer.confectionary.com
    image: hyperledger/fabric-orderer:$IMAGE_TAG
    command: orderer
    # Override environment variables for orderer in this section
    environment:
      - FABRIC_CFG_PATH=/var/hyperledger/config
      - FABRIC_LOGGING_SPEC=ERROR
      - ORDERER_GENERAL_LOCALMSPID=OrdererMSP
    # Host folder mappings
    volumes:
      # Folder with genesis block
      - ${PWD}/config/orderer:/var/hyperledger/config
      # Map the folder with MSP for orderer
      - ${PWD}/config/crypto-config/ordererOrganizations/confectionary.com/orderers/orderer.confectionary.com/msp:/var/hyperledger/msp
      # Added to support the TLS setup
      - ${PWD}/config/crypto-config/ordererOrganizations/confectionary.com/orderers/orderer.confectionary.com/tls:/var/hyperledger/tls
      - data-orderer.confectionary.com:/var/ledger
    ports:
      - 7050:7050
      - 8443:8443
    networks:
      - confec-nets
  
  shop1.confectionary.com:
    container_name: shop1.confectionary.com
    image: hyperledger/fabric-peer:$IMAGE_TAG
    # Override variables in this section
    environment:
      - FABRIC_CFG_PATH=/var/hyperledger/config
      - FABRIC_LOGGING_SPEC=WARNING
      - CORE_PEER_LOCALMSPID=ConfectionaryMSP
      - CORE_VM_DOCKER_HOSTCONFIG_NETWORKMODE=confec-nets
    # Start the peer node
    command: peer node start
    # Host folder mappings
    volumes:
      # Folder with channel create tx file
      - ${PWD}/config/shop1:/var/hyperledger/config
      # Map the folder with MSP for Peer
      - ${PWD}/config/crypto-config/peerOrganizations/confectionary.com/peers/shop1.confectionary.com/msp:/var/hyperledger/msp
      # Added to support the TLS setup
      - ${PWD}/config/crypto-config/peerOrganizations/confectionary.com/peers/shop1.confectionary.com/tls:/var/hyperledger/tls
      - /var/run/:/var/run/
      - data-shop1.confectionary.com:/var/hyperledger/production
    depends_on: 
      - orderer.confectionary.com
    # Map the ports on Host VM to container ports
    ports:
      - 7051:7051
      - 7052:7052
    networks:
      - confec-nets
  
  shop2.confectionary.com:
    container_name: shop2.confectionary.com
    image: hyperledger/fabric-peer:$IMAGE_TAG
    # Override variables in this section
    environment:
      - FABRIC_CFG_PATH=/var/hyperledger/config
      - FABRIC_LOGGING_SPEC=WARNING
      - CORE_PEER_LOCALMSPID=ConfectionaryMSP
      - CORE_VM_DOCKER_HOSTCONFIG_NETWORKMODE=confec-nets
    # Start the peer node
    command: peer node start
    # Host folder mappings
    volumes:
      # Folder with channel create tx file
      - ${PWD}/config/shop2:/var/hyperledger/config
      # Map the folder with MSP for Peer
      - ${PWD}/config/crypto-config/peerOrganizations/confectionary.com/peers/shop2.confectionary.com/msp:/var/hyperledger/msp
      # Added to support the TLS setup
      - ${PWD}/config/crypto-config/peerOrganizations/confectionary.com/peers/shop2.confectionary.com/tls:/var/hyperledger/tls
      - /var/run/:/var/run/
      - data-shop2.confectionary.com:/var/hyperledger/production
    depends_on: 
      - orderer.confectionary.com
    # Map the ports on Host VM to container ports
    ports:
      - 8051:7051
      - 8052:7052
    networks:
      - confec-nets
  
  mill.flourmill.com:
    container_name: mill.flourmill.com
    image: hyperledger/fabric-peer:$IMAGE_TAG
    # landing directory
    working_dir: $HOME
    # command: peer node start --peer-chaincodedev=true
    command: peer node start
    # Environment setup for peer
    environment:
      - FABRIC_CFG_PATH=/var/hyperledger/config
      - FABRIC_LOGGING_SPEC=WARNING
      - CORE_PEER_LOCALMSPID=FlourmillMSP
      - GOPATH=/opt/gopath
      - CORE_VM_DOCKER_HOSTCONFIG_NETWORKMODE=${COMPOSE_PROJECT_NAME}_confec-nets
      - CORE_PEER_GOSSIP_EXTERNALENDPOINT=mill.flourmill.com:7051
    # host file system mappings
    volumes:
      # Folder with channel create tx file
      - ${PWD}/config/mill:/var/hyperledger/config
      # Map the folder with MSP for Peer
      - ${PWD}/config/crypto-config/peerOrganizations/flourmill.com/peers/mill.flourmill.com/msp:/var/hyperledger/msp
      # Added to support the TLS setup
      - ${PWD}/config/crypto-config/peerOrganizations/flourmill.com/peers/mill.flourmill.com/tls:/var/hyperledger/tls
      - /var/run/:/var/run/
      - data-mill.flourmill.com:/var/hyperledger/production
    depends_on: 
      - orderer.confectionary.com
    # Map the ports on Host VM to container ports
    ports:
      - 9051:7051
      - 9052:7052
    networks:
      - confec-nets
  
  ovens.bakingstuff.com:
    container_name: ovens.bakingstuff.com
    image: hyperledger/fabric-peer:$IMAGE_TAG
    # landing directory
    working_dir: $HOME
    # command: peer node start --peer-chaincodedev=true
    command: peer node start
    # Environment setup for peer
    environment:
      - FABRIC_CFG_PATH=/var/hyperledger/config
      - FABRIC_LOGGING_SPEC=WARNING
      - CORE_PEER_LOCALMSPID=BakingstuffMSP
      - GOPATH=/opt/gopath
      - CORE_VM_DOCKER_HOSTCONFIG_NETWORKMODE=${COMPOSE_PROJECT_NAME}_confec-nets
      - CORE_PEER_GOSSIP_EXTERNALENDPOINT=ovens.bakingstuff.com:7051
    # host file system mappings
    volumes:
      # Folder with channel create tx file
      - ${PWD}/config/ovens:/var/hyperledger/config
      # Map the folder with MSP for Peer
      - ${PWD}/config/crypto-config/peerOrganizations/bakingstuff.com/peers/ovens.bakingstuff.com/msp:/var/hyperledger/msp
      # Added to support the TLS setup
      - ${PWD}/config/crypto-config/peerOrganizations/bakingstuff.com/peers/ovens.bakingstuff.com/tls:/var/hyperledger/tls
      - /var/run/:/var/run/
      - data-ovens.bakingstuff.com:/var/hyperledger/production
    depends_on: 
      - orderer.confectionary.com
    # Map the ports on Host VM to container ports
    ports:
      - 10051:7051
      - 10052:7052
    networks:
      - confec-nets